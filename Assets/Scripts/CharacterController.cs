﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public static event System.Action onPlayerAttackResult;

    public Animator animator;
    public GameObject magicBallPrefab;
    public Transform ballSpwnPositionTR;
    public Vector3 ballSpeed;

    private int _score;
    public int Score
    {
        get
        {
            return _score;
        }
        set
        {
            _score = value;
            onScoreChanged?.Invoke();
        }
    }
    public int HighScore
    {
        get
        {
            return PlayerPrefs.GetInt("HS", 0);
        }
        set
        {
            PlayerPrefs.SetInt("HS", value);
            onHighScoreChanged?.Invoke();
        }
    }

    public event System.Action onScoreChanged;
    public event System.Action onHighScoreChanged;

    // Start is called before the first frame update



    void Start()
    {
        WallDetector.onWallWasHit += OnWallHit;
        TargetSpawner.onTargetWasHit += OnTargetHit;
    }
    void OnDestroy()
    {
        WallDetector.onWallWasHit -= OnWallHit;
        TargetSpawner.onTargetWasHit -= OnTargetHit;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnAttackCommand();
        }
    }



    public void OnAnimationAttackPose()
    {
        GameObject magicBallGo = Instantiate(magicBallPrefab);
        magicBallGo.transform.position = ballSpwnPositionTR.position;
        magicBallGo.GetComponent<MagicBall>().Init(ballSpeed);
    }


    void OnAttackCommand()
    {
        animator.SetTrigger("Attack");
    }
    void OnWallHit()
    {
        Debug.Log("wall");
        Score = 0;
        onPlayerAttackResult?.Invoke();
        animator.SetTrigger("Upset");
    }
    void OnTargetHit()
    {
        Debug.Log("target");
        Score += 10;
        if (Score > HighScore)
        {
            HighScore = Score;
        }

        onPlayerAttackResult?.Invoke();
        animator.SetTrigger("Cheer");
    }

}
