﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicBall : MonoBehaviour
{
    public Vector3 speed;

    void Update()
    {
        transform.Translate(speed*Time.deltaTime);
    }

    public void Init(Vector3 speed)
    {
        this.speed = speed;
    }
    private void OnDisable()
    {
        speed = Vector3.zero;
    }

    public void Delete()
    {
        Destroy(gameObject);
    }

}
