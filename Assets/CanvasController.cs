﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CanvasController : MonoBehaviour
{
    public Text scoreText;
    public Text highScoreText;
    public Button restartButton;

    public CharacterController playerUnit;
    // Start is called before the first frame update
    void Start()
    {
        playerUnit.onScoreChanged += ScoreChanged;
        playerUnit.onHighScoreChanged += HighScoreChanged;
        restartButton.onClick.AddListener(Restart);

        ScoreChanged();
        HighScoreChanged();
    }
    void OnDestroy()
    {
        playerUnit.onScoreChanged -= ScoreChanged;
        playerUnit.onHighScoreChanged -= HighScoreChanged;
    }

    void ScoreChanged()
    {
        scoreText.text = string.Format("Score: {0}", playerUnit.Score);
    }
    void HighScoreChanged()
    {
        highScoreText.text = string.Format("HighScore: {0}", playerUnit.HighScore);
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
