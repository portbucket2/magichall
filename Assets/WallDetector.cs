﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDetector : MonoBehaviour
{
    public static event System.Action onWallWasHit;
    private void OnTriggerEnter(Collider other)
    {
        MagicBall magicBall = other.GetComponentInParent<MagicBall>();

        if (magicBall)
        {
            Debug.Log("Fail!");
            onWallWasHit?.Invoke();
            magicBall.Delete();
        }
    }
}
