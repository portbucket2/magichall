﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpawner : MonoBehaviour
{
    public static event System.Action onTargetWasHit;

    public float maxY;
    public float minY;
    public float speed;
    public float speedUpPerRate;
    //public float maxX;
    //public float minX;
    public Transform targetItem;

    private void Start()
    {
        PlayerPrefs.DeleteAll();
        CharacterController.onPlayerAttackResult += IncreaseSpeed;
    }
    void IncreaseSpeed()
    {
        speed += speedUpPerRate;
    }
    private void OnDestroy()
    {
        CharacterController.onPlayerAttackResult -= IncreaseSpeed;
    }

    float direction = 1;
    private void Update()
    {
        
        targetItem.Translate(new Vector3(0,speed*Time.deltaTime*direction,0));
        if (targetItem.localPosition.y > maxY)
        {
            direction = -1;
        }
        if (targetItem.localPosition.y < minY)
        {
            direction = 1;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        MagicBall magicBall = other.GetComponentInParent<MagicBall>();

        if (magicBall)
        {
            Debug.Log("Success!");
            onTargetWasHit?.Invoke();
            magicBall.Delete();
        }
    }
}
